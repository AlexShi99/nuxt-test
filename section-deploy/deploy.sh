#!/bin/bash
urlencode() {
    # urlencode <string>
    old_lc_collate=$LC_COLLATE
    LC_COLLATE=C
    
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c" ;;
        esac
    done
    
    LC_COLLATE=$old_lc_collate
}

git config --global user.email "${APERTURE_USER_EMAIL}"
git config --global user.name "Adore Bitbucket Deploy"

URL_ENCODED_EMAIL=$(urlencode ${APERTURE_USER_EMAIL})
URL_ENCODED_PASSWORD=$(urlencode ${APERTURE_USER_PASSWORD})

git config --global credential.helper store
git clone "https://$URL_ENCODED_EMAIL:$URL_ENCODED_PASSWORD@aperture.section.io/account/863/application/6562/beta.adorebeauty.com.au.git" /src
rm -rf /src/nuxt/

cp -r /nuxt /src

cp /src/nuxt/.env-section-staging /src/nuxt/.env
cp /src/nuxt/.gitignore-section     /src/nuxt/.gitignore

cd /src && git add . && git commit -m "Deploy" && git push origin staging

echo "Deployed to staging"